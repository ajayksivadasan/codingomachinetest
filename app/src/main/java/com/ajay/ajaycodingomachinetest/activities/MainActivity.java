package com.ajay.ajaycodingomachinetest.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ajay.ajaycodingomachinetest.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}