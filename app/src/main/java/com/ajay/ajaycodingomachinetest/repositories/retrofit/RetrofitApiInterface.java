package com.ajay.ajaycodingomachinetest.repositories.retrofit;

import com.ajay.ajaycodingomachinetest.models.OtpGenerateResponseModel;
import com.ajay.ajaycodingomachinetest.models.OtpGenerateSendModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RetrofitApiInterface {
    @POST("oauth/otp")
    Call<OtpGenerateResponseModel> sendOtpToMobile(@Body OtpGenerateSendModel otpGenerateSendModel);
}
