package com.ajay.ajaycodingomachinetest.repositories.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {}, version = 1, exportSchema = false)
public abstract class RoomDb extends RoomDatabase {
    private static final String DATABASE_NAME = "codingo_db";
    private static RoomDb database;

    public synchronized static RoomDb getInstance(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context.getApplicationContext(), RoomDb.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
}
