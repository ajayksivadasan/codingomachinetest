package com.ajay.ajaycodingomachinetest.repositories.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApiClient{
    public static final String BASE_URL = "https://baseapi.com/";
        private static Retrofit retrofit = null;

        private RetrofitApiClient() {
        }

        public static Retrofit getApiClient() {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            // add your other interceptors …
            // add logging as last interceptor
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder().header("Authorization", "key=AAAALD7fwak:APA91bHLYT2cA08Tw5rKdQG3OgsGWXOtykkl-YG9MbaYTylubXeKJJPX5ci-c3HFRFN58wTso_9fUOWaVzXSEQOhAMqNhutkXFawKNykGUng2iP3G-byY6d9HWAwSOmIXsM-Hrg5U7oN");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            });
            httpClient.addInterceptor(logging);
            OkHttpClient client = httpClient.build();
            if (retrofit == null) {
                retrofit = new retrofit2.Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();

            }
            return retrofit;
        }
}
