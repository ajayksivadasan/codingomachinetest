package com.ajay.ajaycodingomachinetest.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class OtpGenerateResponseModel {

    /**
     * success : true
     * message : OTP has been sent successfully.
     * code : OK
     */

    @SerializedName("success")
    private boolean mSuccess;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("code")
    private String mCode;

    public static OtpGenerateResponseModel objectFromData(String str) {

        return new Gson().fromJson(str, OtpGenerateResponseModel.class);
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }
}
