package com.ajay.ajaycodingomachinetest.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class OtpGenerateSendModel {

    /**
     * mobileNumber : +19712052347
     * deviceId : ee5a587c-6478-4a4a-8abc-9fb3d94ec70d
     */

    @SerializedName("mobileNumber")
    private String mMobileNumber;
    @SerializedName("deviceId")
    private String mDeviceId;

    public static OtpGenerateSendModel objectFromData(String str) {

        return new Gson().fromJson(str, OtpGenerateSendModel.class);
    }

    public String getMobileNumber() {
        return mMobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        mMobileNumber = mobileNumber;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }
}
